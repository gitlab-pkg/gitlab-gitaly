# encoding: utf-8

# InSpec tests for gitlab-gitaly package

control 'general-pkg-checks' do
  impact 1.0
  title 'General tests for gitlab-gitaly package'
  desc '
    This control ensures that:
      * gitlab-gitaly package version 0.0.1 is installed

  '
  describe package('gitlab-gitaly') do
    it { should be_installed }
    its ('version') { should eq '0.0.1' }
  end
end

control 'general-binary-checks' do
  impact 1.0
  title 'General tests for gitaly binary'
  desc '
    This control ensures that:
      * gitaly installed in the correct location
      * gitaly have correct shasum
  '
  describe file('/opt/gitaly') do
    it { should exist }
    it { should be_directory }
  end

  describe file('/opt/gitaly/gitaly') do
    it { should exist }
    it { should be_file }
    its('owner') { should eq 'root' }
    its('group') { should eq 'root' }
    its('mode') { should eq 0o755 }
    its('sha256sum') { should eq 'ed681ac782f56a8f3befab957c189ecedfa67ef9242ffc3d22e880e2987cacaf' }
  end

  describe bash('/opt/gitaly/gitaly -version') do
    its('exit_status') { should eq 0 }
    its('stderr.strip') { should eq '' }
    its('stdout.strip') { should include 'Gitaly, version 0.59.2-1-g9225f10-go1.9.2' }
  end
end
