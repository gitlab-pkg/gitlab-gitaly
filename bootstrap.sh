#!/bin/bash -x
# vim: ai:ts=8:sw=8:noet
# This is shell provisioner for Kitchen
set -eufo pipefail
IFS=$'\t\n'


# since gitlab-vault already in repo, install it from there
# and then install the version being tested with dpkg
# lazy way to test repo and fetch all dependencies
echo 'deb http://aptly.gitlab.com/ci xenial main' > /etc/apt/sources.list.d/aptly.gitlab.com.list
# TODO: a proper ssl from a proper CA, but for now just embed the fp here
wget -O /tmp/apt.key http://aptly.gitlab.com/release.asc
echo 'f4fd8d5bc50ab62b42ea6092867a61a9cfe2961c8629ec4591396c7e6e7ce7ab  /tmp/apt.key' | sha256sum --status --check
# shellcheck disable=SC2181
if [[ $? -ne 0 ]]; then
	echo "MiTM in progress, refusing to continue"
	exit $?
fi

apt-key add /tmp/apt.key
apt-get -qq update
dpkg -i /tmp/kitchen/data/gitlab-gitaly_0.0.1.deb
apt-get -yqqf install
